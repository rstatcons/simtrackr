library("lubridate", warn.conflicts = FALSE)
library("modelr")
library("tidyverse", warn.conflicts = FALSE)
options(readr.show_progress = FALSE) # No progress bar while reading csv

# Data import ####

# Read in main csv file
data <- read_delim("raw-data/02_Data_test.csv",
                   ";",
                   col_types =
                     # We want specific columns only
                     cols_only(
                       msisdn = col_double(),
                       imei = col_character(),
                       tstamp = col_double(),
                       long = col_double(),
                       lat = col_double()
                     )) %>%
  # More clear column names
  rename(phonenumb = msisdn,
         device = imei,
         timestamp = tstamp) %>%
  # Convert UNIX epoch to time
  mutate(timestamp = as.POSIXct(timestamp / 1000,
                                origin = "1970-01-01",
                                tz = "Europe/Moscow")) %>%
  # Convert geographic coordinates to kilometers
  mutate(x_km = long * (111.320 * cos(lat * pi / 180)),
         y_km = lat * 110.54)

# Read Excel file
simpairs <- readxl::read_xlsx("raw-data/01_Facts.xlsx",
                              col_names = c("phone1", "phone2")) %>%
  # Add unique ID per each pair of numbers
  mutate(person = row_number()) %>%
  # Split partions into parts for training and testing
  resample_partition(c(test = .3, train = .7)) %>%
  # Convert each part in long format with two columns:
  # ID of pair (person) and phone number
  map(function(df) {
    df$data <- gather_(df$data, "column", "phonenumb", c("phone1", "phone2")) %>%
      select(-column)
    df
  })


# Keep records only with information about pairs
traindata <- data %>%
  inner_join(simpairs$train$data, by = "phonenumb")
